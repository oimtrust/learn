import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("============================= SIMPLE CALCULATOR ===============================");
        System.out.println("===============================================================================");

        Scanner input = new Scanner(System.in);

        // Membaca input nilai pertama
        System.out.println("Input value 1 : ");
        double value1 = input.nextDouble();

        // Membaca input operator
        System.out.println("Input operator ( + - * / % ) : ");
        String operator = input.next();

        // Membaca input nilai kedua
        System.out.println("Input value 2 : ");
        double value2 = input.nextDouble();

        try {
            double result = new Main().getResult(value1, value2, operator);
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            String formattedResult = decimalFormat.format(result);

            System.out.println("Hasil perhitungan dari " + value1 + operator + value2 + " adalah " + formattedResult);
        } catch (IllegalArgumentException e) {
            System.out.println("Operator yang anda masukkan salah!");
        }
    }

    private double getResult(double value1, double value2, String operator) {
        switch (operator) {
            case "+":
                return value1 + value2;

            case "-":
                return value1 - value2;

            case "*":
                return value1 * value2;

            case "/":
                return value1 / value2;

            case "%":
                return value1 % value2;

            default:
                throw new IllegalArgumentException();
        }
    }
}
